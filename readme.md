# Calendar

_Hiatus. Holding off until I've settled the server that will host this and other
projects. Related to the `os` and `proxy` repos._

Planning a CalDAV-driven web calendar. Will be based largely on Toast UI's
Calendar. The TUI cal is beautiful, and deserves some write ups on its
architecture and implementation - see notes/ for those details.

No code has been written yet.
