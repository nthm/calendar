# Frontend

_TOAST UI: Calendar_

Showcase and GIFs at http://ui.toast.com/tui-calendar, and their API is
documented at https://nhnent.github.io/tui.calendar/latest/Calendar.html

Korean, but documentation and notes are in English.

Supports many layouts, multiday events, drag and drop of events, multiple
schedules, timezones, locations, tags, tasks, milestones ...everything. No
frameworks (!?) and, as such, a very interesting project architecture.

It's also actively developed at +1600 commits, last pushed 6 days ago. It has
reason to continue being developed - it's used by an office/cloud suite made by
the same company, "Dooray!" which is an "Issue tracker, Messenger, Wiki, Mail,
Calendar, Drive, Contacts and more".

Contributions are encouraged, and people have made wrappers in Vue, React, and
Angular to make it easier to add to projects.

## Dependencies

-  _tui-code-snippet_ is their bootstrap of useful code snippets. It's
   responsible for their IE8 compatibility as well. Reimplements many helpers.
-  _tui-date-picker (optional)_ and,
-  _tui-time-picker (optional)_ are their components for adding data into the
   calendar. Nice modularity!

## Structure

The docs are lacking, but luckily the codebase is easy to read and has lots of
tests and examples.

It has a similar structure to Leaflet.js, but uses prototypes as a form of
classes to define concepts that extend one another:

```js
var util = require('tui-code-snippet');

var Weekday = require('../weekday'),
    tmpl = require('../template/week/dayGridSchedule.hbs');

/**
 * @constructor
 * @extends {Weekday}
 * @param {object} options - options for DayGridSchedule view
 */
function DayGridSchedule(options, container) {
    Weekday.call(this, options, container);
    this.collapsed = true;
}
util.inherit(DayGridSchedule, Weekday);

/**
 * Render Weekday view
 * @override
 */
DayGridSchedule.prototype.render = function(viewModel) {
    var container = this.container;
    var baseViewModel;

    baseViewModel = this.getBaseViewModel(viewModel);
    container.innerHTML = tmpl(baseViewModel);
    this.fire('afterRender', baseViewModel);
};
```

Which is very component-like. In React-like systems I'd extend a class, and use
hyperscript (via JSX) to render rather than setting innerHTML.

Components are very vanilla, but the _"frontend"_ is written using Handlebars
that is precompiled using a webpack loader. This actually negatively impacts
bundle size, as a 5kb `.hbs` view is compiled to 19kb due to the nature of the
templating engine - more on this later. Components form a tree, and parents tell
their children to render. Rendering is backed by `requestAnimationFrame`.

State is a single store of "schedules" that follows its own JSON schema. There
is no sync to a server, no CalDAV or other.

## Setup

Give it a root and it will mount itself to that. Supports templating, which is
good because that means it wll work with voko.

```js
const calendar = new Calendar('#calendar', {
  defaultView: 'month',
  taskView: true,
  template: {
    monthGridHeader(model) {
      var date = new Date(model.date)
      var template = `<span class="tui-full-grid-date">${date.getDate()}</span>`
      return template
    },
  },
});
```

## Bundle

Here's the graph. The orbiting nodes are common code such as _tui-code-snippet_,
and the heavy modules are listed below.

![](./images/module-graph.png)
![](./images/module-sizes.png)

## Handlebars

Having written voko, I'm interested in views. This project has a folder for
views that uses Handlebars and a webpack loader. You see that in the above code
via `tmpl = require('../template/week/dayGridSchedule.hbs')`. That file:

```hbs
<div
  class="{{CSS_PREFIX}}weekday-schedules {{collapsed}}"
  style="top:{{@root.scheduleContainerTop}}px;">
    {{#each matrices ~}}
    {{#each this ~}} {{! matrix }}
    {{#each this ~}} {{! column }}
    {{#if this ~}} {{! viewmodel }}
    <div
      data-id="{{stamp model}}"
      class="{{CSS_PREFIX}}weekday-schedule-block
        {{#if exceedLeft}} {{CSS_PREFIX}}weekday-exceed-left{{/if}}
        {{#if exceedRight}} {{CSS_PREFIX}}weekday-exceed-right{{/if}}"
      style="top:{{multiply top @root.scheduleBlockHeight}}px;left:{{grid-left this @root.dates}}%;width:{{grid-width this @root.dates}}%">
        <div
          data-schedule-id="{{model.id}}"
          data-calendar-id="{{model.calendarId}}"
          class="{{CSS_PREFIX}}weekday-schedule{{#if model.isFocused}}{{CSS_PREFIX}}weekday-schedule-focused{{/if}}"
            style="height:{{@root.scheduleHeight}}px;line-height:{{@root.scheduleHeight}}px;border-radius:{{@root.styles.borderRadius}};
              {{#if model.isFocused}}
                color:#ffffff;background-color:{{model.color}};border-color:{{model.color}};
              {{else}}
                color:{{model.color}};background-color:{{model.bgColor}}; border-color:{{model.borderColor}};
              {{/if}}
...
```

That's...yeah. Anyway it's 1.68kb. Compiles to this:

```js
var Handlebars = require('/home/tyka/projects/tui.calendar/node_modules/handlebars/runtime.js');
module.exports = (Handlebars['default'] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "\n    "
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "\n    "
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3=container.escapeExpression, alias4="function", alias5=container.lambda;

  return "\n    <div data-id=\""
    + alias3((helpers.stamp || (depth0 && depth0.stamp) || alias2).call(alias1,(depth0 != null ? depth0.model : depth0),{"name":"stamp","hash":{},"data":data}))
    + "\"\n        class=\""
    + alias3(((helper = (helper = helpers.CSS_PREFIX || (depth0 != null ? depth0.CSS_PREFIX : depth0)) != null ? helper : alias2),(typeof helper === alias4 ? helper.call(alias1,{"name":"CSS_PREFIX","hash":{},"data":data}) : helper)))
    + "weekday-schedule-block\n            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.exceedLeft : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n            "
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.exceedRight : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\"\n        style=\"top:"
    + alias3((helpers.multiply || (depth0 && depth0.multiply) || alias2).call(alias1,(depth0 != null ? depth0.top : depth0),((stack1 = (data && data.root)) && stack1.scheduleBlockHeight),{"name":"multiply","hash":{},"data":data}))
    + "px;\n                left:"
    + alias3((helpers["grid-left"] || (depth0 && depth0["grid-left"]) || alias2).call(alias1,depth0,((stack1 = (data && data.root)) && stack1.dates),{"name":"grid-left","hash":{},"data":data}))
    + "%;\n                width:"
    + alias3((helpers["grid-width"] || (depth0 && depth0["grid-width"]) || alias2).call(alias1,depth0,((stack1 = (data && data.root)) && stack1.dates),{"name":"grid-width","hash":{},"data":data}))
...
```

Which doesn't get nearly as far into the original file since it's 10kb! The
output is mostly string concatenation. Other Handlebar files are 20kb...

This impact on the bundle size doesn't even consider the runtime, which you can
see the compiled template taps into. Hard to say what the runtime performance
impact is compared to voko.

Here's the compiler, and webpack loader:
- https://github.com/wycats/handlebars.js/blob/master/lib/handlebars/compiler/compiler.js
- https://github.com/emaphp/handlebars-template-loader
