Repository setup follows:
https://git-scm.com/book/en/v1/Git-Tools-Subtree-Merging

```
> git remote add tui-origin https://github.com/nhnent/tui.calendar
> git fetch tui-origin
> git checkout -b tui-calendar tui-origin/master

> git remote -v
origin  git@gitlab.com:nthm/calendar.git (fetch)
origin  git@gitlab.com:nthm/calendar.git (push)
tui-origin      https://github.com/nhnent/tui.calendar (fetch)
tui-origin      https://github.com/nhnent/tui.calendar (push)

> git read-tree --prefix=tui-calendar -u tui-calendar
Encountered 79 file(s) that should have been pointers, but weren't:
...

> git push --all
```

I thought adding a subtree would be significant size, since the TUI origin is
14MB. However, GitLab only reports a change of 75kb after the above commands.

The pointer warning is OK. It'll be fixed when the LFS objects are committed and
pushed to the new remote (aka here).
