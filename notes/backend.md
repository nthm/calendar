# Backend

This will likely be driven by CalDAV, but may use RemoteStorage if it seems like
a good fit in the future. More research needs to be done on RemoteStorage -
right now it looks like a dead project, but there are some active websites that
support it.

## CalDAV

FOSDEM talk on CalDAV: https://www.youtube.com/watch?v=LzgomnabxdM

HTTP/WebDAV based protocol. Describes how a calendar client can talk to a
server. It's a transport layer - the actual data format is iCal or vCard
(CardDAV) which are text-based, opensource, and standardized.

Uses GET/PUT/DELETE. PUT is also for updating.

They pass around ETags. Which are arbitrary numbers used by the client/server to
know if content is new/changed/worth downloading.


```
GET /server/12200.ics HTTP/1.1
if-none-match: 12200:1

HTTP 200 OK
etag: 12200:1
```

Also for PUT operations:

```
PUT /server/12200.ics HTTP/1.1
if-match: 12200:1

BEGIN:VCALENDAR
...

HTTP 200 OK
etag: 12200:2
```

Note the `:2`. Servers can also reply _419 Conflict_ to indicate that a client
needs to request newer changes before a push can be applied.

You can also say:

```
PUT /server/meeting.ics HTTP/1.1
if-none-match: *

BEGIN:VCALENDAR
...

HTTP 200 OK
etag: 12800:1
location: /server/12800.ics
```

But most of this should be abstracted by Fennel.

Other operations/verbs of WebDAV:
  - PROPFIND
  - LOCK (usually not needed)

WebDAV is all XML...

---

CalDAV implements one verb ontop of WebDAV: REPORT. Is for asking for a query of
events. Ex: one week of data; all events that match a string. For clients that
have a cache, both web and native, it's usually useless because it's less work
to just do the query client side _if_ the client has the data.

There's also MULTIGET which avoid making a ton of seperate GET requests. It
bundles them better, and is less overhead/work.

## Toast UI Conversion

The data for schedules in Toast UI is defined here 
https://nhnent.github.io/tui.calendar/latest/global.html#Schedule and is very
different from the iCal spec that will be used by Radicale/Fennel/CalDAV or
any other servers.

CalDAV will be dealing with iCal formats under the hood, so that above
schedule format will need to be converted. There's an official/valid
converter here https://github.com/mozilla-comm/ical.js

It's 10000 lines, no joke, but would be server-side so it's OK.
